/**
 * Clase Parqueadero, M�todo 1, declarando propiedades para maximoCupoCarros y carros y calculando cupoCarros
 * @author (Milton Jes�s Vera Contreras - miltonjesusvc@ufps.edu.co) 
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @author Colaborador: (Anthony Ardila 11519
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Parqueadero{

    /**Almacena la cantidad de carros que se pueden parquear, la capacidad del Parqueadero para carros*/
    protected int maximoCupoCarros;

    /**Almacena la cantidad de carros parqueados*/  
    protected int carros;    

    /**Almacena el valor a pagar por una hora de parqueadero de carros*/      
    protected int tarifaCarros;        

    /**Almacena el dinero recaudado por carros parqueados*/          
    protected int ingresosCarros;    

    /**Almacena la cantidad de motos que se pueden parquear, la capacidad del Parqueadero para motos*/    
    protected int maximoCupoMotos;    

    /**Almacena la cantidad de motos parqueados*/      
    protected int motos;    

    /**Almacena el valor a pagar por una hora de parqueadero de motos*/          
    protected int ingresosMotos;

    /**Almacena el dinero recaudado por motos parqueados*/              
    protected int tarifaMotos;

    protected int ganancias;
    protected int impuestos;

    /**Cupo maximo por defecto para carros*/
    public static int DEFAULT_MAX_CARROS=10;

    /**Constante con el cupo maximo por defecto para motos*/
    public static int DEFAULT_MAX_MOTOS=10;

    /**Constante con la tarifa por defecto para carros*/
    public static int DEFAULT_TARIFA_CARROS=1000;

    /**Constante con la tarifa por defecto para motos*/
    public static int DEFAULT_TARIFA_MOTOS=500;

    /**
     * Default constructor
     */
    public Parqueadero(){
        /*COMPLETE*/
        this(DEFAULT_MAX_CARROS, DEFAULT_MAX_MOTOS, DEFAULT_TARIFA_CARROS, DEFAULT_TARIFA_MOTOS);
    }

    /**
     * Constructor que recibe unicamente las tarifas
     */
    public Parqueadero(int tarifaCarros, int tarifaMotos){
        /*COMPLETE*/
        this(DEFAULT_MAX_CARROS, DEFAULT_MAX_MOTOS, tarifaCarros, tarifaMotos);
    }

    /**
     * Constructor que recibe todos los parametros
     */
    public Parqueadero(int cupoCarros, int cupoMotos, int tarifaCarros, int tarifaMotos){
        /*COMPLETE*/
        this.maximoCupoCarros = cupoCarros;
        this.maximoCupoMotos = cupoMotos;
        this.tarifaCarros = tarifaCarros;
        this.tarifaMotos = tarifaMotos;
    }

    /*COMPLETE GET y SET solo si aplica segun el diagrama UML*/

    /***Metodo para parquear un carrro (ingresa un carro al parqueadero)*/
    public boolean parquearCarro(){
        if (this.carros < this.maximoCupoCarros) {
            this.carros++;
            return true;
        }
        return false;/*COMPLETE*/      
    }

    /***Metodo para parquear una moto (ingresa una moto al parqueadero)*/
    public boolean parquearMoto(){
        if (this.motos < this.maximoCupoMotos) {
            this.motos++;
            return true;
        }
        return false;/*COMPLETE*/       
    }
    
    private int calcularHoras(int horaEntrada, int horaSalida) {
        int horas = 0;
        if(horaEntrada >=0 && horaEntrada < 24 && horaSalida >= 0 && horaSalida < 24 && horaEntrada <= horaSalida) {

            if(horaSalida > horaEntrada) {
                horas = horaSalida - horaEntrada;
            } else if(horaSalida == horaEntrada) {
                horas = 1;
            } else {
                horaSalida = horaSalida + 1 + 23; // el +1 es por las 12 am que al ser cero no cuenta
                horas = horaSalida - horaEntrada;
            } 
        }
        return horas;
    }

    /***Metodo para retirar un carro del parqueadero*/
    public boolean retirarCarro(int horaEntrada, int horaSalida){
        if(this.carros > 0){
            int horas = this.calcularHoras(horaEntrada, horaSalida);
            if(horas > 0){
                this.ingresosCarros += horas * this.tarifaCarros;
                this.carros--;
                return true;
            }
        }
        return false;/*COMPLETE*/      
    }    

    /***Metodo para retirar una moto del parqueadero*/
    public boolean retirarMoto(int horaEntrada, int horaSalida){//Sim�trico a carros
        if(this.motos > 0){
            int horas = this.calcularHoras(horaEntrada, horaSalida);
            if(horas > 0){
                this.ingresosMotos += horas * this.tarifaMotos;
                this.motos--;
                return true;
            }
        }
        return false;/*COMPLETE*/       
    }        

    //Start GetterSetterExtension Source Code
    
    /**SET Method Propertie tarifaCarros*/
    public void setTarifaCarros(int tarifaCarros){
        this.tarifaCarros = tarifaCarros;
    }//end method setTarifaCarros
    
    /**SET Method Propertie tarifaMotos*/
    public void setTarifaMotos(int tarifaMotos){
        this.tarifaMotos = tarifaMotos;
    }//end method setTarifaMotos
    
    /**SET Method Propertie maximoCupoCarros*/
    public void setMaximoCupoCarros(int maximoCupoCarros){
        if(maximoCupoCarros >= this.carros){
            this.maximoCupoCarros = maximoCupoCarros;
        }
    }//end method setMaximoCupoCarros
    
    /**SET Method Propertie maximoCupoMotos*/
    public void setMaximoCupoMotos(int maximoCupoMotos){
        if(maximoCupoMotos >= this.motos){
            this.maximoCupoMotos = maximoCupoMotos;
        }
    }//end method setMaximoCupoMotos

    /**GET Method Propertie tarifaCarros*/
    public int getTarifaCarros(){
        return this.tarifaCarros;
    }//end method getTarifaCarros
    
    /**GET Method Propertie tarifaMotos*/
    public int getTarifaMotos(){
        return this.tarifaMotos;
    }//end method getTarifaMotos
    
    /**GET Method Propertie maximoCupoCarros*/
    public int getCupoCarros(){
        return this.maximoCupoCarros - this.carros;
    }//end method getMaximoCupoCarros
    
    /**GET Method Propertie maximoCupoMotos*/
    public int getCupoMotos(){
        return this.maximoCupoMotos - this.motos;
    }//end method getMaximoCupoMotos
    
    /**GET Method Propertie maximoCupoCarros*/
    public int getMaximoCupoCarros(){
        return this.maximoCupoCarros;
    }//end method getMaximoCupoCarros

    /**GET Method Propertie maximoCupoMotos*/
    public int getMaximoCupoMotos(){
        return this.maximoCupoMotos;
    }//end method getMaximoCupoMotos    

    /**GET Method Propertie carros*/
    public int getCarros(){
        return this.carros;
    }//end method getCarros

    /**GET Method Propertie motos*/
    public int getMotos(){
        return this.motos;
    }//end method getMotos

    /**GET Method Propertie ingresosCarros*/
    public int getIngresosCarros(){
        return this.ingresosCarros;
    }//end method getIngresosCarros

    /**GET Method Propertie ingresosMotos*/
    public int getIngresosMotos(){
        return this.ingresosMotos;
    }//end method getIngresosMotos

    private int calcularIngresos() {
        return this.ingresosCarros + this.ingresosMotos;
    }
    
    /**GET Method Propertie ganancias*/
    public int getGanancias(){
        return this.calcularIngresos() - this.getImpuestos();
    }//end method getGanancias

    /**GET Method Propertie impuestos*/
    public int getImpuestos(){
        return this.calcularIngresos() * 19 / 100;
    }//end method getImpuestos

    //End GetterSetterExtension Source Code
    //!
}//end class Parqueadero