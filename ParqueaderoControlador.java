/**
 * Write a description of class ParqueaderoControlador here.
 *
 * @author (Jose Florez - florezjoserodolfo@gmail.com)
 * @version (v1.0.0)
 * Sample Skeleton for 'ParqueaderoVista.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class ParqueaderoControlador {

    @FXML // fx:id="brdPanel"
    private BorderPane brdPanel; // Value injected by FXMLLoader

    @FXML // fx:id="txtTarifaC"
    private TextField txtTarifaC; // Value injected by FXMLLoader

    @FXML // fx:id="txtCupoMC"
    private TextField txtCupoMC; // Value injected by FXMLLoader

    @FXML // fx:id="txtParqC"
    private TextField txtParqC; // Value injected by FXMLLoader

    @FXML // fx:id="txtCuposC"
    private TextField txtCuposC; // Value injected by FXMLLoader

    @FXML // fx:id="txtIngresosC"
    private TextField txtIngresosC; // Value injected by FXMLLoader

    @FXML // fx:id="txtTarifaM"
    private TextField txtTarifaM; // Value injected by FXMLLoader

    @FXML // fx:id="txtCupoMM"
    private TextField txtCupoMM; // Value injected by FXMLLoader

    @FXML // fx:id="txtParqM"
    private TextField txtParqM; // Value injected by FXMLLoader

    @FXML // fx:id="txtCuposM"
    private TextField txtCuposM; // Value injected by FXMLLoader

    @FXML // fx:id="txtIngresosM"
    private TextField txtIngresosM; // Value injected by FXMLLoader

    @FXML // fx:id="btnREntradaC"
    private Button btnREntradaC; // Value injected by FXMLLoader

    @FXML // fx:id="btnRSalidaC"
    private Button btnRSalidaC; // Value injected by FXMLLoader

    @FXML // fx:id="btnREntradaM"
    private Button btnREntradaM; // Value injected by FXMLLoader

    @FXML // fx:id="btnRSalidaM"
    private Button btnRSalidaM; // Value injected by FXMLLoader

    @FXML // fx:id="btnCambiarCupos"
    private Button btnCambiarCupos; // Value injected by FXMLLoader

    @FXML // fx:id="btnCambiarTarifas"
    private Button btnCambiarTarifas; // Value injected by FXMLLoader

    @FXML // fx:id="panelActualizar"
    private Pane panelActualizar; // Value injected by FXMLLoader

    @FXML // fx:id="txtActualizar1"
    private TextField txtActualizar1; // Value injected by FXMLLoader

    @FXML // fx:id="lbActualizar1"
    private Label lbActualizar1; // Value injected by FXMLLoader

    @FXML // fx:id="lbActualizar2"
    private Label lbActualizar2; // Value injected by FXMLLoader

    @FXML // fx:id="txtActualizar2"
    private TextField txtActualizar2; // Value injected by FXMLLoader

    @FXML // fx:id="btnCambiarDatos"
    private Button btnCambiarDatos; // Value injected by FXMLLoader

    @FXML // fx:id="btnCancelar"
    private Button btnCancelar; // Value injected by FXMLLoader

    @FXML // fx:id="lbGanancias"
    private Label lbGanancias; // Value injected by FXMLLoader

    @FXML // fx:id="lbImpuestos"
    private Label lbImpuestos; // Value injected by FXMLLoader

    @FXML // fx:id="lbMensaje"
    private Label lbMensaje; // Value injected by FXMLLoader

    // Opcion para actualizar los datos
    // 1 Salida autos - 2 Salida Motos - 3 Cambiar cupos - 4 Cambiar Tarifas
    private int option; 

    private Parqueadero parqueadero;

    public ParqueaderoControlador() {
        parqueadero = new Parqueadero();
    }

    private void actualizarDatos() {
        lbGanancias.setText("Ganancias: $ " + parqueadero.getGanancias());
        lbImpuestos.setText("Impuestos: $ " + parqueadero.getImpuestos());
        // Carros
        txtTarifaC.setText(parqueadero.getTarifaCarros()+"");
        txtCupoMC.setText(parqueadero.getMaximoCupoCarros()+"");
        txtParqC.setText(parqueadero.getCarros()+"");
        txtCuposC.setText(parqueadero.getCupoCarros()+"");
        txtIngresosC.setText(parqueadero.getIngresosCarros()+"");
        // Motos
        txtTarifaM.setText(parqueadero.getTarifaMotos()+"");
        txtCupoMM.setText(parqueadero.getMaximoCupoMotos()+"");
        txtParqM.setText(parqueadero.getMotos()+"");
        txtCuposM.setText(parqueadero.getCupoMotos()+"");
        txtIngresosM.setText(parqueadero.getIngresosMotos()+"");
    }
    
    @FXML
    void rEntradaC() {
        parqueadero.parquearCarro();
        this.actualizarDatos();
    }

    @FXML
    void rEntradaM() {
        parqueadero.parquearMoto();
        this.actualizarDatos();
    }
    
    @FXML
    void rSalidaC() {
        this.showPanel(1, "Hora Entrada C.", "Hora Salida C.");
    }

    @FXML
    void rSalidaM() {
        this.showPanel(2, "Hora Entrada M.", "Hora Salida M.");
    }

    @FXML
    void cambiarCupos() {
        this.showPanel(3, "Cupo Carros", "Cupo Motos");
    }

    @FXML
    void cambiarTarifas() {
        this.showPanel(4, "Tarifa Carros", "Tarifa Motos");
    }

    private void showPanel(int op, String lb1, String lb2) {
        panelActualizar.setVisible(true);
        this.option = op;
        this.lbActualizar1.setText(lb1);
        this.lbActualizar2.setText(lb2);
    }

    @FXML
    void cancelarActualizacion() {
        lbMensaje.setText("");
        panelActualizar.setVisible(false);
    }

    @FXML
    void cambiarDatos() {
        int v1 = Integer.parseInt(txtActualizar1.getText());
        int v2 = Integer.parseInt(txtActualizar2.getText());
        try {
            if(v1>=0 && v2>=0){
                boolean valid = false;
                if(this.option == 1) {
                    valid = !parqueadero.retirarCarro(v1, v2);
                } else if(this.option == 2) {
                    valid = !parqueadero.retirarMoto(v1, v2);
                } else if(this.option == 3) {
                    parqueadero.setMaximoCupoCarros(v1);
                    parqueadero.setMaximoCupoMotos(v2);
                } else if(this.option == 4) {
                    parqueadero.setTarifaCarros(v1);
                    parqueadero.setTarifaMotos(v2);
                }
                if(valid) {
                    lbMensaje.setText("No se puede hacer retiro");
                } else {
                    panelActualizar.setVisible(false);
                    lbMensaje.setText("");
                    this.actualizarDatos();
                }
                txtActualizar1.setText("");
                txtActualizar2.setText("");
            } else {
                lbMensaje.setText("Los dos campos son requeridos");
            }
        } catch(Exception e) {
            lbMensaje.setText("Los datos no son válidos");
        }
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert brdPanel != null : "fx:id=\"brdPanel\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtTarifaC != null : "fx:id=\"txtTarifaC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtCupoMC != null : "fx:id=\"txtCupoMC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtParqC != null : "fx:id=\"txtParqC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtCuposC != null : "fx:id=\"txtCuposC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtIngresosC != null : "fx:id=\"txtIngresosC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtTarifaM != null : "fx:id=\"txtTarifaM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtCupoMM != null : "fx:id=\"txtCupoMM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtParqM != null : "fx:id=\"txtParqM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtCuposM != null : "fx:id=\"txtCuposM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtIngresosM != null : "fx:id=\"txtIngresosM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnREntradaC != null : "fx:id=\"btnREntradaC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnRSalidaC != null : "fx:id=\"btnRSalidaC\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnREntradaM != null : "fx:id=\"btnREntradaM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnRSalidaM != null : "fx:id=\"btnRSalidaM\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnCambiarCupos != null : "fx:id=\"btnCambiarCupos\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnCambiarTarifas != null : "fx:id=\"btnCambiarTarifas\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert panelActualizar != null : "fx:id=\"panelActualizar\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtActualizar1 != null : "fx:id=\"txtActualizar1\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert lbActualizar1 != null : "fx:id=\"lbActualizar1\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert lbActualizar2 != null : "fx:id=\"lbActualizar2\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert txtActualizar2 != null : "fx:id=\"txtActualizar2\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnCambiarDatos != null : "fx:id=\"btnCambiarDatos\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert btnCancelar != null : "fx:id=\"btnCancelar\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert lbGanancias != null : "fx:id=\"lbGanancias\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert lbImpuestos != null : "fx:id=\"lbImpuestos\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";
        assert lbMensaje != null : "fx:id=\"lbMensaje\" was not injected: check your FXML file 'ParqueaderoVista.fxml'.";

        this.actualizarDatos();
    }

}
