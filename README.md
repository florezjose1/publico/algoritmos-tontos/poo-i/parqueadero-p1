### Parqueadero

A continuación el Primer Previo de POO en 2020-I... Se anexan:

[Enunciado](enunciado.pdf)

### NOTA: El test está pensado para que el auto en el parqueadero no pase más de un día, es decir si son las 23, debe salir a las 23, sin embargo, si quiere dejar que pueda salir a las 01, 02 o más del día siguiente, solo actualice la validación en el método `calcularHoras`:

```java
if(horaEntrada >=0 && horaEntrada < 24 && horaSalida >= 0 && horaSalida < 24 && horaEntrada <= horaSalida) {...}
```

por:

```java
if(horaEntrada >=0 && horaEntrada < 24 && horaSalida >= 0 && horaSalida < 24) {...}
```

Al hacer esto, podrá retirar vehículos dejando pasar hasta el día siguiente una hora antes de la hora de entrada.

Tenga en cuenta que al hacer esta modificación, dos test van a fallar: `Métodos senior y Retirar Junior`



Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)

